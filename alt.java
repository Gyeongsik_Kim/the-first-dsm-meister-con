package meisterproblem1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.security.CodeSource;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.ProtectionDomain;
import java.util.Scanner;

public class MeisterProblem1
{
  private static boolean auth;
  private static File orginal;

  public static void main(String[] args)
    throws InterruptedException, IOException, URISyntaxException, NoSuchAlgorithmException
  {
    orginal = new File(MeisterProblem1.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
    auth = true;

    new Thread(new Runnable() {
      public void run() {
        while (true) {
          MeisterProblem1.auth = true;
          try
          {
            Thread.sleep(1000L);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
    }).start();
    String hash = notRelatedMethod(orginal);

    Scanner scanner = new Scanner(System.in);

    System.out.println("<< -- MeisterCUP LOGIN -- >>");
    System.out.print("Enter ID : ");
    String id = scanner.nextLine();

    System.out.print("Enter PW : ");
    String pw = testMD5(scanner.nextLine());

    System.out.println("\n\n\n");
    scanner.close();

    new Thread(new Runnable(hash, id, pw) {
      public void run() {
        while (true) {
          if (MeisterProblem1.auth) {
            try {
              System.out.println(MeisterProblem1.access$2("http://meister.play-ultimate.net/problem/programming/Authentication/auth.php?hash=" + 
              MeisterProblem1.this + "&id=" + this.val$id + "&password=" + this.val$pw));
            } catch (IOException e) {
              e.printStackTrace();
            }
          }

          System.out.println("Failed Authentication!");
          try {
            Thread.sleep(100L);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
    }).start();
  }

  private static String getUrlSource(String url) throws IOException {
    URL site = new URL(url);
    URLConnection connection = site.openConnection();
    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));

    StringBuilder builder = new StringBuilder();
    String inputLine;
    while ((inputLine = reader.readLine()) != null)
    {
      String inputLine;
      builder.append(inputLine);
    }reader.close();

    return builder.toString();
  }

  private static String testMD5(String str) {
    String MD5 = "";
    try {
      MessageDigest md = MessageDigest.getInstance("MD5");
      md.update(str.getBytes());
      byte[] byteData = md.digest();
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < byteData.length; i++) {
        sb.append(Integer.toString((byteData[i] & 0xFF) + 256, 16).substring(1));
      }
      MD5 = sb.toString();
    }
    catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
      MD5 = null;
    }
    return MD5;
  }

  private static String getFileChecksum(MessageDigest digest, File file) throws IOException {
    FileInputStream fis = new FileInputStream(file);

    byte[] byteArray = new byte[1024];
    int bytesCount = 0;

    while ((bytesCount = fis.read(byteArray)) != -1) {
      digest.update(byteArray, 0, bytesCount);
    }

    fis.close();

    byte[] bytes = digest.digest();

    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < bytes.length; i++)
    {
      sb.append(Integer.toString((bytes[i] & 0xFF) + 256, 16).substring(1));
    }

    return sb.toString();
  }

  public static String notRelatedMethod(File file) throws NoSuchAlgorithmException, IOException {
    return getFileChecksum(MessageDigest.getInstance("MD5"), file);
  }
}