import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by GyungDal on 2015-12-10.
 */
public class Study_13_7 {
    public static void main(String[] args) throws IOException {
        int i;
        Scanner scan = new Scanner(System.in);
        System.out.print("입력 파일명을 입력하세요 : ");
        String s = scan.next();
        FileInputStream fos = new FileInputStream(s);
        while((i = fos.read()) != 1) {
            System.out.println(i);
        }
        fos.close();
        System.out.println(s + "파일명으로 바이트 파일 출력 완료");
    }
}
