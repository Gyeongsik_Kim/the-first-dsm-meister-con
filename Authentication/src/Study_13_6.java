import meisterproblem1.MeisterProblem1;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by GyungDal on 2015-12-10.
 */
public class Study_13_6 {
    public static void main(String[] args) throws IOException, NoSuchFieldException, InterruptedException, NoSuchAlgorithmException, URISyntaxException {
        MeisterProblem1 m = new MeisterProblem1();
        m.main(null);
        Class clazz = m.getClass();
        while (true) {
        try{
            Field auth = clazz.getDeclaredField("auth");
            auth.setAccessible(true);
            auth.set(m,true);
        }catch(Exception e){
            e.printStackTrace();
        }
        }
    }
}